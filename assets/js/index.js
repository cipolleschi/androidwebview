(function(){
	
	var newsList = [];
	
	$("#homePage").on("pagebeforeshow", function(){
		console.log("init ajaxOpt");
		
		var ajaxOpt = {
			url : "http://turismo.comune.cremona.it/it/news/json",
			//url : "file:///android_asset/news.json",
			type : "GET",
			//jsonpCallback : "newsResponse",
			//jsonp : "callback",
			//dataType : "jsonp",
			dataType : "json",
			//accept : "application/json",
			success : function(data, status, jqXHR){
				console.log("success!");
				console.log(JSON.stringify(data));
				
				var nodes = data.nodes;
				$("#news-listview").empty();
				for(var n=0; n < nodes.length; n++){
					var node = nodes[n].node;
					var news = {};
					
					news.image = node.field_news_image_fid;
					news.title = node.title;
					news.desc = node.body;
					news.header = node.teaser;
					news.url = node.path;
					news.id = node.Nid;
					newsList.push(news);
					
					var toAppend = "<li><a href='#' onclick='goToNews(\""+news.id+"\")'>";
					toAppend += "<img src='"+news.image+"' />";
					toAppend += "<h1>"+news.title+"</h1>";
					toAppend += "<p>"+news.header+"</p>";
					toAppend += "</a></li>";
					$("#news-listview").append(toAppend);
					
				}
				$("#news-listview").listview();
				$("#news-listview").listview("refresh");
				
			}, error : function(jqXHR, status, error){
				console.log("Error: "+status + " - " + error);
			}
		};
		console.log("calling ajax");
		$.ajax(ajaxOpt);
		console.log("Ajax called");	
			
	});	
	
	function goToNews(id){
		var n = null;
		for(var i=0; i < newsList.length; i++){
			var tmp = newsList[i];
			if(tmp.id == id){
				n = tmp;
				break;
			}
		}
		if(n == null){
			console.error("News not found");
			return;
		}
		
		$("#title-cont").html(n.title);
		$("#image-cont").attr("src", n.image);
		$("#desc-cont").html(n.desc);
		
		$.mobile.navigate("#detailPage");
		
		
	};
	
	window.goToNews = goToNews;
})();
