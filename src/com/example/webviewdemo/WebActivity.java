package com.example.webviewdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;

public class WebActivity extends Activity {

	private WebView _webView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Questa riga di codice riumove il titolo dall'app
        //In questo modo possiamo gestire tutto con l'html
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //Questa riga imposta il layout che abbiamo definito
        //nell'editor grafico
        setContentView(R.layout.activity_web);
        
        //Ottengo il riferimento alla web view
        _webView = (WebView)findViewById(R.id.webView1);
        //Abilito l'esecuzione di codice JS
        //Senza questa istruzione, il codice JS non si pu� eseguire!
        _webView.getSettings().setJavaScriptEnabled(true);
        _webView.getSettings().setAllowFileAccessFromFileURLs(true);
        _webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        //Carico i file locali
        _webView.loadUrl("file:///android_asset/index.html");
        
        
    }
}
